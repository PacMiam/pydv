# ------------------------------------------------------------------------------
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 3 of the License.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
# ------------------------------------------------------------------------------

# ------------------------------------------------------------------------------
#   Modules
# ------------------------------------------------------------------------------

# Filesystem
from os.path import exists
from os.path import abspath
from os.path import basename
from os.path import expanduser
from os.path import join as path_join

from shutil import copy2 as copy

# Logging
import logging

# Regex
from re import match

# System
from sys import exit as sys_exit

# ------------------------------------------------------------------------------
#   Modules - pyDV
# ------------------------------------------------------------------------------

from pydv import PyDV

from pydv.display import Display
from pydv.interface import MainInterface

from pydv.utils import get_data
from pydv.configuration import Configuration

# ------------------------------------------------------------------------------
#   Modules - Gobject
# ------------------------------------------------------------------------------

try:
    from gi import require_version

    require_version("Gdk", "3.0")

    from gi.repository import Gdk

except ImportError as error:
    sys_exit("Cannot import python3-gobject in document: %s" % str(error))

# ------------------------------------------------------------------------------
#   Class
# ------------------------------------------------------------------------------

class Application(object):

    def __init__(self, args):
        """ Constructor
        """

        # ----------------------------------------
        #   Variables
        # ----------------------------------------

        # Document metadata
        self.path = args.document
        self.page = int(args.page) - 1

        # Logger
        self.logger = logging.getLogger(__name__)

        # Monitors
        self.displays = list()

        # ----------------------------------------
        #   Initialization
        # ----------------------------------------

        # Found main monitors
        self.__init_monitors()

        # Show available monitors
        if args.list_monitors:

            for display in self.displays:
                self.logger.info("Found %d monitor(s) on display %s" % (
                    len(display.get_monitors()), display.get_name()))

                for monitor in display.get_monitors():
                    self.logger.info("\t- %s" % str(monitor))

        # Launch application
        else:
            # A PDF document has been set
            if self.path is not None:
                self.path = expanduser(abspath(self.path))

            # Main display
            self.display_id = args.display
            # Manage default monitors
            self.monitor_id = args.monitor
            self.main_monitor_id = args.main_monitor

            if args.monitor is None:
                self.monitor_id = args.main_monitor

            # ----------------------------------------
            #   Check timer
            # ----------------------------------------

            self.timer = None

            if len(args.timer) > 0 and match(r'^\d{1,2}:\d{1,2}$', args.timer):
                hours, minutes = args.timer.split(':')

                # Parse timer constant as integer
                self.timer = (int(hours), int(minutes))

                # Hours
                if self.timer[0] < 0:
                    self.timer[0] = 0
                elif self.timer[0] > 7:
                    self.timer[0] = 7

                # Minutes
                if self.timer[1] < 0:
                    self.timer[1] = 0
                elif self.timer[1] > 59:
                    self.timer[1] = 59

            # Initialize configurations
            self.__init_configurations()

            # Start application
            self.__start_application()


    def __init_monitors(self):
        """ Initialize monitors detection
        """

        self.logger.info("Initialize monitors")

        display_manager = Gdk.DisplayManager.get()

        for display in display_manager.list_displays():
            self.logger.debug("Found display %s" % display.get_name())

            self.displays.append(Display(display))


    def __init_configurations(self):
        """ Initialize configurations files
        """

        self.config = dict()

        for path in [ PyDV.Path.CONFIG, PyDV.Path.PROFILE ]:

            if not exists(path):
                self.logger.info(
                    "Copy default %s configuration file" % basename(path))

                copy(get_data(path_join("config", basename(path))), path)

            self.config[basename(path)] = Configuration(path)

            # Get missing keys from current config
            self.config[basename(path)].add_missing_data(
                get_data(path_join("config", basename(path))))


    def __start_application(self):
        """ Start and manage main interfaces
        """

        self.logger.info("Start application")

        try:
            display = self.get_display(self.display_id)

            # Check display
            if display is not None:

                # Get primary monitor
                if self.main_monitor_id is not None:
                    main_monitor = display.get_monitor(self.main_monitor_id)

                else:
                    main_monitor = display.get_default_monitor()

                # Get monitor object for presentation
                if self.monitor_id is not None:
                    second_monitor = display.get_monitor(self.monitor_id)

                else:
                    second_monitor = main_monitor

                self.logger.debug(
                    "Use %s as main monitor" % main_monitor.get_name())
                self.logger.debug(
                    "Use %s as secondary monitor" % second_monitor.get_name())

                # Launch main interface
                self.interface = MainInterface(
                    self, main_monitor, second_monitor)

        except TypeError as error:
            self.logger.exception("Wrong monitor type:", str(error))

        except KeyError as error:
            self.logger.exception("Cannot found monitor %s" % self.monitor_id)

        except Exception as error:
            self.logger.exception("An Exception occurs:", str(error))


    def get_display(self, index):
        """ Get a display from a specific index

        Parameters
        ----------
        index : int
            Display index

        Returns
        -------
        pydv.display.Display / None
            The main display if available

        Examples
        --------
        >>> self.get_display(0)
        <pydv.display.Display object at 0x7fa0c462f7b8>
        """

        if not index in range(len(self.displays)):
            return None

        return self.displays[index]
