# ------------------------------------------------------------------------------
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 3 of the License.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
# ------------------------------------------------------------------------------

# ------------------------------------------------------------------------------
#   Modules
# ------------------------------------------------------------------------------

# Date
from datetime import datetime
from datetime import timedelta

# Filesystem
from os import utime
from os import mkdir
from os import makedirs

from os.path import exists
from os.path import abspath
from os.path import dirname
from os.path import basename
from os.path import splitext
from os.path import expanduser
from os.path import join as path_join

from tempfile import gettempdir

# Logging
import logging

# Processus
from subprocess import PIPE
from subprocess import Popen
from subprocess import STDOUT

# System
from os import environ
from sys import exit as sys_exit
from shlex import split as shlex_split

# URL
from urllib.error import URLError
from urllib.request import urlretrieve

# ------------------------------------------------------------------------------
#   Modules - pyDV
# ------------------------------------------------------------------------------

from pydv.utils import *
from pydv.presentation import PresentationInterface

# ------------------------------------------------------------------------------
#   Modules - gobject
# ------------------------------------------------------------------------------

try:
    from gi import require_version

    require_version("Gtk", "3.0")

    from gi.repository import Gtk
    from gi.repository import Gdk
    from gi.repository import GLib

    from gi.repository.GLib import idle_add
    from gi.repository.GLib import source_remove

    from gi.repository.GdkPixbuf import Pixbuf
    from gi.repository.GdkPixbuf import Colorspace
    from gi.repository.GdkPixbuf import InterpType

    require_version("GtkSource", "3.0")

    from gi.repository import GtkSource

    require_version("Vte", "2.91")

    from gi.repository import Vte

    require_version("EvinceView", "3.0")

    from gi.repository import EvinceView
    from gi.repository import EvinceDocument

    # Initialize Evince backend
    EvinceDocument.init()

except ImportError as error:
    sys_exit("Import error with python3-gobject module: %s" % str(error))

# ------------------------------------------------------------------------------
#   Modules - Translation
# ------------------------------------------------------------------------------

from gettext import gettext as _
from gettext import textdomain
from gettext import bindtextdomain

bindtextdomain("pydv", get_data("i18n"))
textdomain("pydv")

# ------------------------------------------------------------------------------
#   Class
# ------------------------------------------------------------------------------

class MainInterface(Gtk.Window):

    def __init__(self, parent, main_monitor, second_monitor):
        """ Constructor
        """

        Gtk.Window.__init__(self)

        if not "pydv.conf" in parent.config:
            raise IOError("Cannot found pydv.conf")

        if not "profile.conf" in parent.config:
            raise IOError("Cannot found profile.conf")

        # ------------------------------------
        #   Initialize variables
        # ------------------------------------

        # Application instance
        self.application = parent

        # Interface monitor
        self.main_monitor = main_monitor
        self.second_monitor = second_monitor

        # Configuration file
        self.config = parent.config["pydv.conf"]

        # Profile file
        self.profile = parent.config["profile.conf"]

        # Logger
        self.logger = logging.getLogger(__name__)

        # Document path
        self.__current_path = parent.path

        # Current page
        self.__current_page = parent.page
        # Current notebook tab
        self.__current_tab = int()

        # Thread variables
        self.__thread_thumbnails = None

        # Treeiter cache
        self.__treeiter_cache = list()
        self.__monitors_cache = dict()

        # Files cache
        self.__files_cache = dict()
        self.__files_allowed_extensions = list()

        # Annotations cache
        self.__annotation_status = False
        self.__annotations_cache = list()

        # Timer mode
        self.__timer = parent.timer
        self.__timer_mode = False
        self.__timer_pause = False
        self.__timer_value = None

        # Search mode
        self.__previous_search = None

        # ------------------------------------
        #   Prepare interface
        # ------------------------------------

        # Init widgets
        self.__init_widgets()

        # Init packing
        self.__init_packing()

        # Init signals
        self.__init_signals()

        # Start interface
        self.__start_interface()


    def __init_widgets(self):
        """ Initialize interface widgets
        """

        # ------------------------------------
        #   Main window
        # ------------------------------------

        self.set_title(_("Main Interface"))

        self.set_default_size(1440, 900)

        self.set_wmclass("pydv", "pyDV")

        self.set_gravity(Gdk.Gravity.CENTER)

        # ------------------------------------
        #   Grid
        # ------------------------------------

        self.scroll_image = Gtk.ScrolledWindow()
        self.viewport_image = Gtk.Viewport()

        self.scroll_links = Gtk.ScrolledWindow()
        self.viewport_links = Gtk.Viewport()

        self.scroll_sidebar = Gtk.ScrolledWindow()

        self.box = Gtk.Box()
        self.box_content = Gtk.Box()
        self.box_notebook_actions = Gtk.Box()

        self.box_popover_menu = Gtk.Box()
        self.box_popover_application = Gtk.Box()

        self.box_popover_timer = Gtk.Box()
        self.box_popover_timer_play = Gtk.Box()
        self.box_popover_timer_stop = Gtk.Box()
        self.box_popover_timer_define = Gtk.Box()
        self.box_popover_timer_buttons = Gtk.Box()

        self.box_toolbar_navigation = Gtk.Box()

        # Properties
        self.scroll_sidebar.set_policy(
            Gtk.PolicyType.NEVER, Gtk.PolicyType.AUTOMATIC)

        self.box.set_spacing(0)
        self.box.set_orientation(Gtk.Orientation.VERTICAL)

        self.box_content.set_spacing(0)
        self.box_content.set_orientation(Gtk.Orientation.HORIZONTAL)

        self.box_popover_menu.set_orientation(Gtk.Orientation.VERTICAL)
        self.box_popover_menu.set_border_width(6)
        self.box_popover_menu.set_spacing(6)

        self.box_popover_application.set_orientation(Gtk.Orientation.VERTICAL)
        self.box_popover_application.set_border_width(6)
        self.box_popover_application.set_spacing(6)

        self.box_popover_timer.set_orientation(Gtk.Orientation.VERTICAL)
        self.box_popover_timer.set_border_width(12)
        self.box_popover_timer.set_spacing(6)

        self.box_popover_timer_play.set_spacing(4)
        self.box_popover_timer_play.set_halign(Gtk.Align.CENTER)
        self.box_popover_timer_play.set_orientation(
            Gtk.Orientation.HORIZONTAL)

        self.box_popover_timer_stop.set_spacing(4)
        self.box_popover_timer_stop.set_halign(Gtk.Align.CENTER)
        self.box_popover_timer_stop.set_orientation(
            Gtk.Orientation.HORIZONTAL)

        self.box_popover_timer_buttons.set_spacing(6)
        self.box_popover_timer_buttons.set_orientation(
            Gtk.Orientation.HORIZONTAL)

        Gtk.StyleContext.add_class(
            self.box_popover_timer_define.get_style_context(), "linked")
        self.box_popover_timer_define.set_spacing(-1)
        self.box_popover_timer_define.set_orientation(
            Gtk.Orientation.HORIZONTAL)

        Gtk.StyleContext.add_class(
            self.box_toolbar_navigation.get_style_context(), "linked")
        self.box_toolbar_navigation.set_spacing(-1)
        self.box_toolbar_navigation.set_orientation(
            Gtk.Orientation.HORIZONTAL)

        # ------------------------------------
        #   Files filter
        # ------------------------------------

        self.__files_filter_pdf = Gtk.FileFilter.new()
        self.__files_filter_documents = Gtk.FileFilter.new()

        # Properties
        self.__files_filter_pdf.add_pattern("*.pdf")

        # ------------------------------------
        #   Clipboard
        # ------------------------------------

        self.clipboard = Gtk.Clipboard.get(Gdk.Atom.intern("CLIPBOARD", False))

        # ------------------------------------
        #   Overlay
        # ------------------------------------

        self.overlay = Gtk.Overlay()

        # ------------------------------------
        #   Headerbar
        # ------------------------------------

        self.headerbar = Gtk.HeaderBar()

        self.headerbar_image_application = Gtk.Image()
        self.headerbar_button_application = Gtk.MenuButton()

        self.headerbar_image_menu = Gtk.Image()
        self.headerbar_button_menu = Gtk.MenuButton()

        self.headerbar_button_timer = Gtk.MenuButton()

        # Properties
        self.headerbar.set_show_close_button(True)

        self.headerbar_image_application.set_from_icon_name(
            "x-office-presentation-symbolic", Gtk.IconSize.BUTTON)
        self.headerbar_button_application.set_image(
            self.headerbar_image_application)

        self.headerbar_image_menu.set_from_icon_name(
            "open-menu-symbolic", Gtk.IconSize.BUTTON)
        self.headerbar_button_menu.set_image(self.headerbar_image_menu)

        self.headerbar_button_timer.set_relief(Gtk.ReliefStyle.NONE)

        self.headerbar.set_custom_title(self.headerbar_button_timer)

        # ------------------------------------
        #   Headerbar - Timer
        # ------------------------------------

        self.headerbar_label = Gtk.Label()

        self.popover_timer = Gtk.Popover()

        self.image_popover_timer_play = Gtk.Image()
        self.label_popover_timer_play = Gtk.Label()
        self.button_popover_timer_play = Gtk.Button()

        self.image_popover_timer_stop = Gtk.Image()
        self.label_popover_timer_stop = Gtk.Label()
        self.button_popover_timer_stop = Gtk.Button()

        self.label_popover_timer_define = Gtk.Label()
        self.label_popover_timer_define_separator = Gtk.Label()
        self.spin_popover_timer_define_hours = Gtk.SpinButton()
        self.spin_popover_timer_define_minutes = Gtk.SpinButton()
        self.adjustment_popover_timer_define_hours = Gtk.Adjustment()
        self.adjustment_popover_timer_define_minutes = Gtk.Adjustment()

        # Properties
        self.popover_timer.set_modal(True)

        self.headerbar_button_timer.add(self.headerbar_label)
        self.headerbar_button_timer.set_popover(self.popover_timer)

        self.headerbar_label.set_use_markup(True)

        self.image_popover_timer_play.set_from_icon_name(
            "media-playback-start", Gtk.IconSize.MENU)
        self.label_popover_timer_play.set_label(_("Play"))
        self.label_popover_timer_play.set_halign(Gtk.Align.START)
        self.label_popover_timer_play.set_valign(Gtk.Align.CENTER)

        self.button_popover_timer_play.set_tooltip_text(_("Play"))
        self.button_popover_timer_play.add(
            self.box_popover_timer_play)
        self.button_popover_timer_play.get_style_context().add_class(
            "suggested-action")

        self.image_popover_timer_stop.set_from_icon_name(
            "media-playback-stop", Gtk.IconSize.MENU)
        self.label_popover_timer_stop.set_label(_("Stop"))
        self.label_popover_timer_stop.set_halign(Gtk.Align.START)
        self.label_popover_timer_stop.set_valign(Gtk.Align.CENTER)

        self.button_popover_timer_stop.set_tooltip_text(_("Stop"))
        self.button_popover_timer_stop.add(
            self.box_popover_timer_stop)
        self.button_popover_timer_stop.get_style_context().add_class(
            "destructive-action")

        self.label_popover_timer_define.set_margin_top(12)
        self.label_popover_timer_define.set_label(_("Timer"))
        self.label_popover_timer_define.set_halign(Gtk.Align.CENTER)
        self.label_popover_timer_define.set_valign(Gtk.Align.CENTER)
        self.label_popover_timer_define.get_style_context().add_class(
            "dim-label")

        self.label_popover_timer_define_separator.set_label('∶')
        self.label_popover_timer_define_separator.set_margin_left(4)
        self.label_popover_timer_define_separator.set_margin_right(4)
        self.label_popover_timer_define_separator.get_style_context().add_class(
            "dim-label")

        self.adjustment_popover_timer_define_hours.set_lower(0.0)
        self.adjustment_popover_timer_define_hours.set_upper(7.0)
        self.adjustment_popover_timer_define_hours.set_page_increment(2.0)
        self.adjustment_popover_timer_define_hours.set_step_increment(1.0)

        self.spin_popover_timer_define_hours.set_halign(Gtk.Align.END)
        self.spin_popover_timer_define_hours.set_adjustment(
            self.adjustment_popover_timer_define_hours)

        self.adjustment_popover_timer_define_minutes.set_lower(0.0)
        self.adjustment_popover_timer_define_minutes.set_upper(59.0)
        self.adjustment_popover_timer_define_minutes.set_page_increment(5.0)
        self.adjustment_popover_timer_define_minutes.set_step_increment(1.0)

        self.spin_popover_timer_define_minutes.set_halign(Gtk.Align.START)
        self.spin_popover_timer_define_minutes.set_adjustment(
            self.adjustment_popover_timer_define_minutes)

        # ------------------------------------
        #   Headerbar - Menu
        # ------------------------------------

        self.popover_menu = Gtk.Popover()

        # Properties
        self.popover_menu.set_modal(True)

        self.headerbar_button_menu.set_popover(self.popover_menu)

        # ------------------------------------
        #   Headerbar - Application
        # ------------------------------------

        self.popover_application = Gtk.Popover()

        self.listbox_application = Gtk.ListBox()

        # Properties
        self.popover_application.set_modal(True)
        self.popover_application.set_border_width(6)

        self.listbox_application.set_selection_mode(Gtk.SelectionMode.SINGLE)
        self.listbox_application.set_activate_on_single_click(True)

        self.headerbar_button_application.set_popover(self.popover_application)

        # ------------------------------------
        #   Toolbar
        # ------------------------------------

        self.toolbar = Gtk.Toolbar()

        self.toolbar_item_open = Gtk.ToolItem()
        self.toolbar_open = Gtk.Button()

        self.toolbar_item_freeze = Gtk.ToolItem()
        self.toolbar_freeze = Gtk.ToggleButton()

        self.toolbar_item_highlight = Gtk.ToolItem()
        self.toolbar_highlight = Gtk.Button()

        self.toolbar_item_remove = Gtk.ToolItem()
        self.toolbar_remove = Gtk.Button()

        self.toolbar_item_mouse = Gtk.ToolItem()
        self.toolbar_mouse = Gtk.ToggleButton()

        self.toolbar_item_search = Gtk.ToolItem()
        self.toolbar_search = Gtk.SearchEntry()

        self.toolbar_item_pages = Gtk.ToolItem()
        self.toolbar_pages = Gtk.ComboBoxText()

        self.toolbar_item_navigation = Gtk.ToolItem()
        self.toolbar_page_next = Gtk.Button()
        self.toolbar_page_previous = Gtk.Button()

        self.image_open = Gtk.Image()
        self.image_mouse = Gtk.Image()
        self.image_freeze = Gtk.Image()
        self.image_remove = Gtk.Image()
        self.image_highlight = Gtk.Image()
        self.image_page_next = Gtk.Image()
        self.image_page_previous = Gtk.Image()

        self.toolbar_item_expand = Gtk.SeparatorToolItem()

        # Properties
        self.toolbar_open.set_image(self.image_open)
        self.toolbar_mouse.set_image(self.image_mouse)
        self.toolbar_freeze.set_image(self.image_freeze)
        self.toolbar_remove.set_image(self.image_remove)
        self.toolbar_highlight.set_image(self.image_highlight)
        self.toolbar_page_next.set_image(self.image_page_next)
        self.toolbar_page_previous.set_image(self.image_page_previous)

        self.toolbar_freeze.set_label(_("Freeze"))
        self.toolbar_freeze.get_style_context().add_class(
            "suggested-action")

        self.toolbar_search.set_icon_from_icon_name(
            Gtk.EntryIconPosition.SECONDARY, "edit-clear-symbolic")

        self.image_open.set_from_icon_name(
            "document-open-symbolic", Gtk.IconSize.SMALL_TOOLBAR)
        self.image_mouse.set_from_icon_name(
            "input-mouse-symbolic", Gtk.IconSize.SMALL_TOOLBAR)
        self.image_freeze.set_from_icon_name(
            "media-playback-pause-symbolic", Gtk.IconSize.SMALL_TOOLBAR)
        self.image_remove.set_from_icon_name(
            "edit-clear-all-symbolic", Gtk.IconSize.SMALL_TOOLBAR)
        self.image_highlight.set_from_icon_name(
            "insert-text-symbolic", Gtk.IconSize.SMALL_TOOLBAR)
        self.image_page_next.set_from_icon_name(
            "go-next-symbolic", Gtk.IconSize.SMALL_TOOLBAR)
        self.image_page_previous.set_from_icon_name(
            "go-previous-symbolic", Gtk.IconSize.SMALL_TOOLBAR)

        self.toolbar_pages.set_wrap_width(4)

        self.toolbar_item_expand.set_expand(True)
        self.toolbar_item_expand.set_draw(False)

        # ------------------------------------
        #   Notebook
        # ------------------------------------

        self.notebook = Gtk.Notebook()

        self.notebook_actions_add_button = Gtk.Button()
        self.notebook_actions_add_image = Gtk.Image()

        self.notebook_label_document = Gtk.Label()

        # Properties
        self.notebook.set_scrollable(True)

        self.notebook_actions_add_image.set_from_icon_name(
            "tab-new-symbolic", Gtk.IconSize.BUTTON)
        self.notebook_actions_add_button.set_image(
            self.notebook_actions_add_image)
        self.notebook_actions_add_button.set_relief(Gtk.ReliefStyle.NONE)

        self.notebook_label_document.set_label(_("Document"))

        # ------------------------------------
        #   Document
        # ------------------------------------

        self.view_document = EvinceView.View.new()
        self.model_document = EvinceView.DocumentModel()

        # Properties
        self.view_document.set_allow_links_change_zoom(False)

        self.model_document.set_page_layout(EvinceView.PageLayout.SINGLE)
        self.model_document.set_sizing_mode(EvinceView.SizingMode.BEST_FIT)
        self.model_document.set_continuous(False)
        self.model_document.set_dual_page(False)

        # ------------------------------------
        #   Sidebar
        # ------------------------------------

        self.thumbnails_list = Gtk.ListStore(Pixbuf, str)

        self.thumbnails_view = Gtk.IconView.new()

        self.pixbuf_loading = icon_load("image-loading", 150)

        # Properties
        self.thumbnails_view.set_activate_on_single_click(True)
        self.thumbnails_view.set_model(self.thumbnails_list)
        self.thumbnails_view.set_item_width(150)
        self.thumbnails_view.set_pixbuf_column(0)
        self.thumbnails_view.set_text_column(1)
        self.thumbnails_view.set_columns(1)
        self.thumbnails_view.set_spacing(6)

        # ------------------------------------
        #   Statusbar
        # ------------------------------------

        self.statusbar = Gtk.Statusbar()

        self.statubar_label_page = Gtk.Label()
        self.statubar_progress_page = Gtk.ProgressBar()

        # Properties
        self.statusbar.get_message_area().get_style_context().add_class(
            "dim-label")

        self.statubar_progress_page.set_orientation(Gtk.Orientation.HORIZONTAL)


    def __init_packing(self):
        """ Initialize widgets packing in main window
        """

        # ------------------------------------
        #   Headerbar
        # ------------------------------------

        self.set_titlebar(self.headerbar)

        self.headerbar.pack_start(self.headerbar_button_application)
        self.headerbar.pack_end(self.headerbar_button_menu)

        self.statusbar.get_message_area().pack_end(
            self.statubar_label_page, False, False, 0)

        # ------------------------------------
        #   Popover - Menu
        # ------------------------------------

        self.popover_menu.add(self.box_popover_menu)

        # ------------------------------------
        #   Popover - Application
        # ------------------------------------

        self.popover_application.add(self.listbox_application)

        # ------------------------------------
        #   Popover - Timer
        # ------------------------------------

        self.popover_timer.add(self.box_popover_timer)

        self.box_popover_timer.pack_start(
            self.box_popover_timer_buttons, False, False, 0)
        self.box_popover_timer.pack_start(
            self.label_popover_timer_define, False, False, 0)
        self.box_popover_timer.pack_start(
            self.box_popover_timer_define, False, False, 0)

        self.box_popover_timer_define.pack_start(
            self.spin_popover_timer_define_hours, False, False, 0)
        self.box_popover_timer_define.pack_start(
            self.label_popover_timer_define_separator, False, False, 0)
        self.box_popover_timer_define.pack_start(
            self.spin_popover_timer_define_minutes, False, False, 0)

        self.box_popover_timer_play.pack_start(
            self.image_popover_timer_play, False, False, 0)
        self.box_popover_timer_play.pack_start(
            self.label_popover_timer_play, False, False, 0)

        self.box_popover_timer_stop.pack_start(
            self.image_popover_timer_stop, False, False, 0)
        self.box_popover_timer_stop.pack_start(
            self.label_popover_timer_stop, False, False, 0)

        self.box_popover_timer_buttons.pack_start(
            self.button_popover_timer_play, True, True, 0)
        self.box_popover_timer_buttons.pack_start(
            self.button_popover_timer_stop, True, True, 0)

        # ------------------------------------
        #   Toolbar
        # ------------------------------------

        self.toolbar.insert(self.toolbar_item_pages, -1)
        self.toolbar.insert(Gtk.SeparatorToolItem(), -1)
        self.toolbar.insert(self.toolbar_item_navigation, -1)
        self.toolbar.insert(Gtk.SeparatorToolItem(), -1)
        self.toolbar.insert(self.toolbar_item_open, -1)
        self.toolbar.insert(Gtk.SeparatorToolItem(), -1)
        self.toolbar.insert(self.toolbar_item_freeze, -1)
        self.toolbar.insert(self.toolbar_item_expand, -1)
        self.toolbar.insert(self.toolbar_item_mouse, -1)
        self.toolbar.insert(Gtk.SeparatorToolItem(), -1)
        self.toolbar.insert(self.toolbar_item_highlight, -1)
        self.toolbar.insert(self.toolbar_item_remove, -1)
        self.toolbar.insert(Gtk.SeparatorToolItem(), -1)
        self.toolbar.insert(self.toolbar_item_search, -1)

        self.toolbar_item_open.add(self.toolbar_open)
        self.toolbar_item_mouse.add(self.toolbar_mouse)
        self.toolbar_item_freeze.add(self.toolbar_freeze)
        self.toolbar_item_remove.add(self.toolbar_remove)
        self.toolbar_item_highlight.add(self.toolbar_highlight)

        self.toolbar_item_search.add(self.toolbar_search)

        self.toolbar_item_pages.add(self.toolbar_pages)

        self.box_toolbar_navigation.pack_start(
            self.toolbar_page_previous, False, False, 0)
        self.box_toolbar_navigation.pack_start(
            self.toolbar_page_next, False, False, 0)

        self.toolbar_item_navigation.add(self.box_toolbar_navigation)

        # ------------------------------------
        #   Document
        # ------------------------------------

        self.box_notebook_actions.pack_start(
            self.notebook_actions_add_button, False, False, 0)

        self.notebook.set_action_widget(
            self.box_notebook_actions, Gtk.PackType.END)

        self.notebook.append_page(
            self.overlay, self.notebook_label_document)
        self.notebook.set_tab_reorderable(self.overlay, True)

        self.scroll_image.add(self.view_document)

        self.overlay.add(self.scroll_image)

        # ------------------------------------
        #   Sidebar
        # ------------------------------------

        self.scroll_sidebar.add(self.thumbnails_view)

        # ------------------------------------
        #   Statusbar
        # ------------------------------------

        self.statusbar.get_message_area().pack_end(
            self.statubar_progress_page, False, False, 0)

        # ------------------------------------
        #   Main Window
        # ------------------------------------

        self.box_content.pack_start(self.notebook, True, True, 0)
        self.box_content.pack_start(self.scroll_sidebar, False, False, 0)

        self.box.pack_start(self.toolbar, False, False, 0)
        self.box.pack_start(self.box_content, True, True, 0)
        self.box.pack_end(self.statusbar, False, False, 0)

        self.add(self.box)


    def __init_signals(self):
        """ Initialize widgets signals
        """

        self.connect(
            "destroy", self.on_stop_interface)
        self.connect(
            "key-press-event", self.__on_manage_keys)

        self.signal_thumbnails = self.thumbnails_view.connect(
            "item-activated", self.on_select_thumbnail)

        self.signal_annotation = self.view_document.connect(
            "annot-added", self.__on_added_annotation)
        self.view_document.connect(
            "annot-removed", self.__on_removed_annotation)
        self.view_document.connect(
            "external-link", self.on_external_link_clicked)
        # self.view_document.connect(
            # "motion-notify-event", self.__on_manage_pointer_position)

        self.signal_page = self.model_document.connect(
            "page-changed", self.on_presentation_change_page)

        self.button_popover_timer_play.connect(
            "clicked", self.__on_start_timer)
        self.button_popover_timer_stop.connect(
            "clicked", self.__on_stop_timer)

        self.toolbar_page_next.connect(
            "clicked", self.__on_next_page)
        self.toolbar_page_previous.connect(
            "clicked", self.__on_previous_page)
        self.toolbar_open.connect(
            "clicked", self.on_open_document)
        self.toolbar_highlight.connect(
            "clicked", self.__on_start_annotation)
        self.toolbar_remove.connect(
            "clicked", self.__on_remove_all_annotations)

        self.toolbar_freeze.connect(
            "toggled", self.__on_freeze_document)

        self.toolbar_search.connect(
            "icon-press", self.__on_cancel_search)
        self.toolbar_search.connect(
            "activate", self.__on_search)

        self.signal_pages = self.toolbar_pages.connect(
            "changed", self.on_select_combobox)

        self.notebook_actions_add_button.connect(
            "clicked", self.on_append_new_file)

        self.notebook.connect(
            "switch-page", self.on_switch_notebook_page)


    def __start_interface(self):
        """ Load data and start interface
        """

        self.__signals = {
            self.toolbar_pages: self.signal_pages,
            self.model_document: self.signal_page,
            self.view_document: self.signal_annotation,
            self.thumbnails_view: self.signal_thumbnails,
        }

        # ------------------------------------
        #   Available display
        # ------------------------------------

        radio_group = None

        for display in self.application.displays:

            for monitor in display.get_monitors():
                radio = Gtk.RadioButton.new_with_label_from_widget(
                    radio_group, monitor.get_name())
                radio.set_border_width(6)

                row = Gtk.ListBoxRow()
                row.add(radio)

                self.listbox_application.add(row)

                self.__monitors_cache[radio] = (row, monitor)

                if monitor == self.second_monitor:
                    self.listbox_application.select_row(row)

                    radio.set_active(True)

                if radio_group is None:
                    radio_group = radio

                radio.connect("toggled", self.__on_select_new_monitor)

        # ------------------------------------
        #   Allowed extensions
        # ------------------------------------

        if self.profile is not None:
            self.logger.info("Retrieve allowed extensions")

            for section in self.profile.sections():
                if self.profile.has_option(section, "extensions"):
                    extensions = self.profile.get(
                        section, "extensions", fallback=str()).split()

                    for extension in extensions:
                        if not extension in self.__files_allowed_extensions:
                            self.__files_allowed_extensions.append(extension)

                            self.__files_filter_documents.add_pattern(
                                "*.%s" % str(extension))

            self.logger.debug(
                "Found %d extensions" % len(self.__files_allowed_extensions))

        # ------------------------------------
        #   Timer
        # ------------------------------------

        if self.__timer is not None:
            self.spin_popover_timer_define_hours.set_value(self.__timer[0])
            self.spin_popover_timer_define_minutes.set_value(self.__timer[1])

        self.button_popover_timer_stop.set_sensitive(False)

        # Set default time in headerbar
        self.__on_update_timer()
        GLib.timeout_add(1000, self.__on_update_timer)

        # ------------------------------------
        #   Document
        # ------------------------------------

        # Launch presentation interface
        self.presentation = PresentationInterface(self)

        try:
            self.__on_load_document(self.__current_page)

        except ValueError as error:
            self.logger.exception(
                "ValueError occurs during document opening: %s" % str(error))

        except IOError as error:
            self.logger.exception(
                "IOError occurs during document opening: %s" % str(error))

        # ------------------------------------
        #   Sidebar
        # ------------------------------------

        position = self.config.get(
            "interface", "sidebar-position", fallback="right")

        if position == "left":
            self.box_content.reorder_child(self.scroll_sidebar, 0)

        # ------------------------------------
        #   Main Window
        # ------------------------------------

        self.logger.info("Show interface")

        try:
            width, height = self.config.get(
                "windows", "main", fallback="1024x768").split('x')

            self.set_default_size(int(width), int(height))
            self.resize(int(width), int(height))

        except ValueError as error:
            self.logger.exception("Cannot resize main window: %s" % str(error))

            self.set_default_size(1024, 768)

        self.set_position(Gtk.WindowPosition.CENTER)
        self.unrealize()

        # Unactive some buttons
        self.toolbar_mouse.set_sensitive(False)
        self.toolbar_remove.set_sensitive(False)

        self.toolbar_page_next.set_sensitive(False)
        self.toolbar_page_previous.set_sensitive(False)

        self.toolbar_freeze.set_sensitive(False)
        self.toolbar_remove.set_sensitive(False)
        self.toolbar_highlight.set_sensitive(False)

        # Draw the result on screen
        self.show_all()
        self.box_popover_menu.show_all()
        self.box_popover_timer.show_all()
        self.box_popover_application.show_all()
        self.box_notebook_actions.show_all()
        self.listbox_application.show_all()

        self.scroll_sidebar.hide()
        self.statubar_progress_page.hide()

        # Move window on monitor center
        x, y = self.main_monitor.get_position()
        width, height = self.main_monitor.get_size()

        self.move(
            x + int(width / 2) - int(self.get_size()[0] / 2),
            y + int(height / 2) - int(self.get_size()[1] / 2))

        Gtk.main()


    def on_stop_interface(self, *args):
        """ Save data and stop interface
        """

        self.logger.info("Closing application")

        # Stop thumbnails generator thread
        if self.__thread_thumbnails is not None:
            self.logger.info("Closing remaining thread")
            source_remove(self.__thread_thumbnails)

        Gtk.main_quit()


    def __on_manage_keys(self, widget, event):
        """ Manage widgets for specific keymaps

        Parameters
        ----------
        widget : Gtk.Widget
            Object which receive signal
        event : Gdk.EventButton or Gdk.EventKey
            Event which triggered this signal
        """

        # Fullscreen mode
        if event.keyval == Gdk.KEY_F11:
            pass


    def __on_start_annotation(self, widget):
        """ Start annotation process

        Parameters
        ----------
        widget : Gtk.Widget
            Object which receive signal
        """

        self.block_signal()

        self.__annotation_status = True

        self.view_document.begin_add_annotation(
            EvinceDocument.AnnotationType.TEXT_MARKUP)

        self.unblock_signal()


    def __on_added_annotation(self, widget, annotation):
        """ An annotation has been added to interface

        Parameters
        ----------
        widget : Gtk.Widget
            Object which receive signal
        annotation : EvinceDocument.Annotation
            Annotation which has been added
        """

        # Re-render thumbnail with annotation
        # self.__on_render_single_thumbnail(self.__current_page)

        # Show the annotation in presentation document
        self.presentation.view_presentation.reload()

        if not annotation in self.__annotations_cache:
            self.__annotations_cache.append(annotation)

        self.__annotation_status = False

        self.toolbar_remove.set_sensitive(True)


    def __on_removed_annotation(self, widget, annotation):
        """ An annotation has been added to interface

        Parameters
        ----------
        widget : Gtk.Widget
            Object which receive signal
        annotation : EvinceDocument.Annotation
            Annotation which has been added
        """

        if annotation in self.__annotations_cache:
            self.__annotations_cache.remove(annotation)

        if len(self.__annotations_cache) > 0:
            self.toolbar_remove.set_sensitive(True)

        self.__annotation_status = False


    def __on_remove_all_annotations(self, widget):
        """ Remove all annotations from document

        Parameters
        ----------
        widget : Gtk.Widget
            Object which receive signal
        """

        for annotation in self.__annotations_cache:
            self.view_document.remove_annotation(annotation)

        self.toolbar_remove.set_sensitive(False)

        self.view_document.reload()
        self.presentation.view_presentation.reload()


    def __on_load_document(self, page=int()):
        """ Load a document in main interface

        Parameters
        ----------
        page : int, optional
            Page to show on the current document

        Raises
        ------
        ValueError
            When path type is not a string
        IOError
            When the spcified path not exist
        """

        self.__files_cache = dict()

        self.__annotation_status = False

        # Remove previous annotations
        self.__annotations_cache.clear()
        # Remove previous thumbnails cache
        self.__treeiter_cache.clear()

        # Remove previous tab pages
        for filename, widget in self.__files_cache.items():
            self.on_close_tab(None, widget)

        if self.__current_path is not None:

            if not exists(expanduser(self.__current_path)):
                raise IOError(2, "Cannot found %s" % str(self.__current_path))

            self.toolbar_freeze.set_sensitive(True)
            self.toolbar_highlight.set_sensitive(True)

            # ----------------------------------------
            #   Generate document instance
            # ----------------------------------------

            self.thumbnails_view.unselect_all()
            self.thumbnails_list.clear()

            self.logger.info(
                "Retrieve document %s" % basename(self.__current_path))

            self.__current_page = page

            self.logger.debug("Start on page %d" % (self.__current_page + 1))

            # Generate a document from filepath
            self.document = EvinceDocument.Document.factory_get_document(
                "file://%s" % abspath(self.__current_path))

            # Generate presentation instance
            self.presentation.init_document(self.document, self.__current_page)

            # ----------------------------------------
            #   Generate document model
            # ----------------------------------------

            self.logger.debug("Generate a new model for this document")
            self.model_document.set_document(self.document)

            self.logger.debug("Set this model to evince viewer")
            self.view_document.set_model(self.model_document)

            # ----------------------------------------
            #   Thumbnails
            # ----------------------------------------

            # Stop thumbnails generator thread
            if self.__thread_thumbnails is not None:
                self.logger.info("Closing remaining thread")
                source_remove(self.__thread_thumbnails)

            # Render document page thumbnails
            loader = self.__on_render_thumbnails()
            self.__thread_thumbnails = idle_add(loader.__next__)

            # ----------------------------------------
            #   Load widgets
            # ----------------------------------------

            if self.__current_page <= 0:
                self.toolbar_page_previous.set_sensitive(False)

            # Show the document URI in statusbar
            self.statusbar.push(0, basename(self.document.get_uri()))

            # Render first document page
            self.on_change_page()


    def __on_render_thumbnails(self):
        """ Render document thumbnails

        Notes
        -----
        Using yield avoid an UI freeze when append a lot of thumbnails
        """

        size = self.config.getint("interface", "thumbnails-size", fallback=400)

        self.logger.debug("Starting thumbnails renderer thread")
        self.scroll_sidebar.show_all()

        # Restore statusbar visibility
        self.statubar_progress_page.show()

        self.__treeiter_cache.clear()

        # Reset pages combobox list
        self.toolbar_pages.handler_block(self.signal_pages)
        self.toolbar_pages.remove_all()
        self.toolbar_pages.handler_unblock(self.signal_pages)

        # Store an empty iter to thumbnails list
        for page in range(0, self.document.get_n_pages()):
            self.toolbar_pages.append_text(str(page + 1))

            self.__treeiter_cache.append(
                self.thumbnails_list.append(
                    [self.pixbuf_loading, str(page + 1)]
                )
            )

            # Select the correct page
            if page == self.__current_page:
                self.toolbar_pages.set_active(page)

                self.thumbnails_view.select_path(self.thumbnails_list.get_path(
                    self.__treeiter_cache[page]))

        # Generate thumbnail render for every page
        for page in range(0, self.document.get_n_pages()):

            self.statubar_progress_page.set_fraction(
                float(page / (self.document.get_n_pages())))

            self.__on_render_single_thumbnail(page, size)

            yield True

        self.__thread_thumbnails = None

        # Hide statusbar now the render is done
        self.statubar_progress_page.hide()

        yield False


    def __on_render_single_thumbnail(self, page, size=None):
        """ Render a single thumbnail from document

        Parameters
        ----------
        page : int
            Document page index
        size : int, optional
            Thumbnail size
        """

        if page >= 0:

            if size is None:
                size = self.config.getint(
                    "interface", "thumbnails-size", fallback=400)

            # Retrieve specified page thumbnail
            pixbuf = self.document.get_thumbnail(
                EvinceDocument.RenderContext.new(
                    self.document.get_page(page), 0, 1.0))

            # Rescale to scroll current size
            ratio = min(
                float(size / pixbuf.get_width()),
                float(size / pixbuf.get_height()))

            # Rescale pixbuf
            pixbuf = self.document.get_thumbnail(
                EvinceDocument.RenderContext.new(
                    self.document.get_page(page), 0, ratio))

            # Append thumbnail to sidebar icons view
            self.thumbnails_list.set(self.__treeiter_cache[page], 0, pixbuf)


    def __on_update_timer(self):
        """ Update timer every time the thread is called

        Returns
        -------
        bool
            Thread return status
        """

        if not self.__timer_pause:
            date = datetime.now()

            if self.__timer_mode:
                if self.__timer_value.total_seconds() > 0:
                    self.__timer_value -= timedelta(seconds=1)

                    date = datetime(1, 1, 1) + self.__timer_value

                else:
                    self.__on_stop_timer()

        else:
            date = datetime(1, 1, 1) + self.__timer_value

        text = "<tt><span size='xx-large'>%02d∶%02d</span>∶%02d</tt>" % (
            date.hour,
            date.minute,
            date.second
        )

        self.headerbar_label.set_markup(text)

        return True


    def __on_start_timer(self, widget):
        """ Manage start and pause timer functions

        This function allow to switch between play and pause status in timer
        popover

        Parameters
        ----------
        widget : Gtk.Widget
            Object which received the signal
        """

        # Start a new timer based on spinbutton value
        if self.__timer_value is None:
            hours = self.spin_popover_timer_define_hours.get_value_as_int()
            minutes = self.spin_popover_timer_define_minutes.get_value_as_int()

            if minutes > 0 or hours > 0:
                self.__timer_mode = True
                self.__timer_pause = False

                self.__timer_value = timedelta(hours=hours, minutes=minutes)

                self.image_popover_timer_play.set_from_icon_name(
                    "media-playback-pause", Gtk.IconSize.MENU)
                self.label_popover_timer_play.set_label(_("Pause"))

                self.button_popover_timer_stop.set_sensitive(True)
                self.spin_popover_timer_define_hours.set_sensitive(False)
                self.spin_popover_timer_define_minutes.set_sensitive(False)

        # Manage pause
        elif not self.__timer_pause:
            self.__timer_pause = True

            self.image_popover_timer_play.set_from_icon_name(
                "media-playback-start", Gtk.IconSize.MENU)
            self.label_popover_timer_play.set_label(_("Play"))

        elif self.__timer_pause:
            self.__timer_pause = False

            self.image_popover_timer_play.set_from_icon_name(
                "media-playback-pause", Gtk.IconSize.MENU)
            self.label_popover_timer_play.set_label(_("Pause"))


    def __on_stop_timer(self, widget=None):
        """ Manage stop timer function

        This function allow to stop current timer and reset widgets status

        Others Parameters
        -----------------
        widget : Gtk.IconView
            Object which received the signal
        """

        self.__timer_mode = False
        self.__timer_pause = False

        self.__timer_value = None

        self.image_popover_timer_play.set_from_icon_name(
            "media-playback-start", Gtk.IconSize.MENU)
        self.label_popover_timer_play.set_label(_("Play"))

        self.spin_popover_timer_define_hours.set_sensitive(True)
        self.spin_popover_timer_define_minutes.set_sensitive(True)
        self.button_popover_timer_stop.set_sensitive(False)


    def __on_select_new_monitor(self, widget):
        """ Select a new monitor where show presentation widget

        Parameters
        ----------
        widget : Gtk.Widget
            Object which received the signal
        """

        if widget.get_active() and widget in self.__monitors_cache.keys():
            row, monitor = self.__monitors_cache[widget]

            self.listbox_application.select_row(row)

            self.presentation.change_display(monitor)


    def __on_manage_pointer_position(self, widget, event):
        """ Manage the pointer position when laser mode is active

        Parameters
        ----------
        widget : Gtk.Widget
            Object which received the signal
        event : Gdk.EventButton or Gdk.EventKey
            Event which triggered this signal
        """

        if self.toolbar_mouse.get_active():

            # Deprecated: Use Gdk.Window.get_device_position() instead.
            x, y = self.view_document.get_pointer()

            area, baseline = self.view_document.get_allocated_size()

            self.presentation.move_cursor(
                int((area.width + 1) / (x + 1)) - 1,
                int((area.height + 1) / (y + 1)) - 1)


    def __on_freeze_document(self, widget):
        """ Freeze presentation

        Parameters
        ----------
        widget : Gtk.Widget
            Object which received the signal
        """

        if widget.get_active():
            widget.set_label(_("Restore"))
            widget.get_style_context().remove_class("suggested-action")
            widget.get_style_context().add_class("destructive-action")

            self.image_freeze.set_from_icon_name(
                "media-playback-start-symbolic", Gtk.IconSize.SMALL_TOOLBAR)

        else:
            widget.set_label(_("Freeze"))
            widget.get_style_context().remove_class("destructive-action")
            widget.get_style_context().add_class("suggested-action")

            self.image_freeze.set_from_icon_name(
                "media-playback-pause-symbolic", Gtk.IconSize.SMALL_TOOLBAR)

            self.presentation.update_page(self.__current_page)

            self.on_switch_notebook_page(self.notebook,
                self.notebook.get_nth_page(self.__current_tab),
                self.__current_tab, restore=True)


    def __on_search(self, widget):
        """ Search into document

        Parameters
        ----------
        widget : Gtk.Widget
            Object which received the signal
        """

        entry = self.toolbar_search.get_text()

        try:
            if not self.__previous_search == entry:

                job = EvinceView.JobFind.new(self.document, self.__current_page,
                    self.document.get_n_pages() - self.__current_page,
                    entry, False)

                if job.has_results:
                    self.__previous_search = entry

                    self.view_document.find_started(job)

                else:
                    self.__previous_search = None

            elif self.__previous_search is not None:
                if not self.view_document.find_search_changed():
                    self.view_document.find_next()
                    self.view_document.find_set_highlight_search(True)

        except Exception as error:
            self.logger.exception(error)


    def __on_cancel_search(self, widget, pos, event):
        """ Cancel search

        Parameters
        ----------
        widget : Gtk.Entry
            Entry widget
        pos : Gtk.EntryIconPosition
            Position of the clicked icon
        event : Gdk.EventButton or Gdk.EventKey
            Event which triggered this signal
        """

        try:
            if self.__previous_search is not None:
                self.__previous_search = None

                self.view_document.find_cancel()

        except Exception as error:
            self.logger.exception(error)


    def __on_next_page(self, widget):
        """ Go to the next page

        Parameters
        ----------
        widget : Gtk.Widget
            Object which received the signal
        """

        self.view_document.next_page()


    def __on_previous_page(self, widget):
        """ Go to the previous page

        Parameters
        ----------
        widget : Gtk.Widget
            Object which received the signal
        """

        self.view_document.previous_page()


    def on_change_page(self):
        """ Change current page
        """

        self.block_signal()

        page = self.__current_page

        self.statubar_label_page.set_text(
            "%d / %d" % (page + 1, self.document.get_n_pages()))

        if page in range(self.document.get_n_pages()):
            self.model_document.set_page(page)

            # Update presentation page
            if not self.toolbar_freeze.get_active():
                self.presentation.update_page(page)

            # Update combobox pages selector
            self.toolbar_pages.set_active(page)

            # Select current page in thumbnails iconview
            if page in range(0, len(self.__treeiter_cache)):
                path = self.thumbnails_list.get_path(
                    self.__treeiter_cache[page])

                if path is not None:
                    self.thumbnails_view.scroll_to_path(path, False, 0.5, 0.5)

                    self.thumbnails_view.select_path(path)

        # Manage previous page button
        if self.__current_page <= 0:
            self.toolbar_page_previous.set_sensitive(False)
        else:
            self.toolbar_page_previous.set_sensitive(True)

        # Manage next page button
        if self.__current_page >= self.document.get_n_pages() - 1:
            self.toolbar_page_next.set_sensitive(False)
        else:
            self.toolbar_page_next.set_sensitive(True)

        self.unblock_signal()


    def on_select_thumbnail(self, widget, path):
        """ Update the current page from iconview

        Parameters
        ----------
        widget : Gtk.Widget
            Object which received the signal
        path : Gtk.TreePath
            Path for the activated item
        """

        model = widget.get_model()

        treeiter = model.get_iter(path)

        if treeiter is not None:
            page = int(model.get_value(treeiter, 1)) - 1

            if not self.__current_page == page:
                self.__current_page = page

                if self.config.getboolean(
                    "interface", "switch-on-click", fallback=False):

                    page = self.notebook.page_num(self.overlay)

                    if not page == self.notebook.get_current_page():
                        self.notebook.set_current_page(page)

                self.on_change_page()


    def on_select_combobox(self, widget):
        """ Select a page from combobox

        Parameters
        ----------
        widget : Gtk.Widget
            Object which received the signal
        """

        self.__current_page = widget.get_active()

        self.on_change_page()


    def on_switch_notebook_page(self, notebook, page, page_num, restore=False):
        """ Update presentation widgets from selected notebook tab

        Parameters
        ----------
        notebook : Gtk.Notebook
            The object which received the signal
        page : Gtk.Widget
            The new current page
        page_num : int
            The index of the page
        restore : bool, optional
            Restore the interface after a freeze
        """

        self.block_signal()

        if restore or not page_num == self.__current_tab:
            self.__current_tab = page_num

            if not self.toolbar_freeze.get_active():

                if type(page) is InterfaceTab:
                    self.presentation.update_file_buffer(
                        page.filepath, page.get_buffer())

                    if page.vte_show_output_switch.get_active():
                        self.presentation.set_visibility(
                            self.presentation.scroll_output)

                    else:
                        self.presentation.set_visibility(
                            self.presentation.scroll_editor)

                        page.scroll_buffer()

                else:
                    self.presentation.set_visibility(
                        self.presentation.overlay)

        self.unblock_signal()


    def on_open_document(self, widget):
        """ Open a new document

        Parameters
        ----------
        widget : Gtk.Widget
            Object which received the signal
        """

        path = self.__current_path

        self.popover_menu.hide()
        self.set_sensitive(False)

        dialog = Gtk.FileChooserDialog(use_header_bar=True)

        dialog.set_action(Gtk.FileChooserAction.OPEN)
        dialog.set_filter(self.__files_filter_pdf)
        dialog.set_select_multiple(False)
        dialog.set_create_folders(True)
        dialog.set_transient_for(self)

        if self.__current_path is not None:
            dialog.set_filename(self.__current_path)

        dialog.add_button(_("Cancel"), Gtk.ResponseType.CANCEL)
        dialog.add_button(_("Accept"), Gtk.ResponseType.ACCEPT)

        if dialog.run() == Gtk.ResponseType.ACCEPT:

            if not dialog.get_filename() == self.__current_path:
                path = dialog.get_filename()

        dialog.destroy()

        if not path == self.__current_path:
            self.__current_path = path

            self.__on_load_document()

        self.set_sensitive(True)


    def on_external_link_clicked(self, widget, action):
        """ Emit when an external link is clicked in document

        Parameters
        ----------
        widget : EvinceView.ViewPresentation
            The object which received the signal
        action : EvinceDocument.LinkAction
            The link action data
        """

        loader = self.on_download_file(action.get_uri())

        idle_add(loader.__next__)


    def on_download_file(self, url):
        """ Retrieve a resource via URL

        Parameters
        ----------
        url : str
            Resource URL
        """

        yield True

        self.logger.info("Check resources from %s" % str(url))

        try:
            # Generate tempdir
            temp = path_join(gettempdir(), "pydv")
            if not exists(temp):
                self.logger.debug("Create pydv tempdir %s" % str(temp))
                mkdir(temp)

            filename = path_join(temp, basename(url))

            # Avoid to store on temp folder
            if len(basename(filename)) > 0:
                name, extension = splitext(basename(filename))

                # Check if the resource extension can be downloaded
                if extension[1:] in self.__files_allowed_extensions:

                    # Retrieve resource
                    if not exists(filename):
                        self.logger.info(
                            "Retrieve %s file" % str(basename(filename)))

                        urlretrieve(url, filename, self.on_download_progress)

                    # Open file if necessary
                    self.on_append_tab(filename)

        except URLError as error:
            self.logger.exception(
                "Cannot access to %s: %s" % (str(url), str(error)))

        yield False


    def on_download_progress(self, count, size, total_size):
        """ Hook function that will be called by urlretrieve

        Parameters
        ----------
        count : int
            Count of blocks transferred so far
        size : bytes
            Block size
        total_size : int
            File total size
        """

        yield True


    def on_append_tab(self, filepath):
        """ Open a file in a new tab if not exists

        Parameters
        ----------
        filepath : str
            File path on disk
        """

        if exists(filepath) and not filepath in self.__files_cache.keys():
            box = InterfaceTab(self, filepath)

            box.editor_buffer.connect(
                "changed", self.on_modify_editor, box)
            box.vte.connect(
                "contents-changed", self.on_modify_output, box)
            box.button_close.connect(
                "clicked", self.on_close_tab, box)

            self.notebook.append_page(box, box.grid)
            self.notebook.set_tab_reorderable(box, True)

            self.__files_cache[filepath] = box

        else:
            # Get widget page from notebook tab
            page = self.notebook.page_num(self.__files_cache[filepath])
            if not page == -1 and not page == self.notebook.get_current_page():
                self.notebook.set_current_page(page)


    def on_append_new_file(self, widget):
        """ Open a new file on notebook tab

        Parameters
        ----------
        widget : Gtk.Button
            The object which received the signal
        """

        self.popover_menu.hide()
        self.set_sensitive(False)

        dialog = Gtk.FileChooserDialog(use_header_bar=True)

        dialog.set_action(Gtk.FileChooserAction.SAVE)
        dialog.set_filter(self.__files_filter_documents)
        dialog.set_select_multiple(False)
        dialog.set_create_folders(True)
        dialog.set_transient_for(self)

        dialog.add_button(_("Cancel"), Gtk.ResponseType.CANCEL)
        dialog.add_button(_("Accept"), Gtk.ResponseType.ACCEPT)

        if dialog.run() == Gtk.ResponseType.ACCEPT:
            path = dialog.get_filename()

            if not exists(dirname(path)):
                makedirs(dirname(path))

            if not exists(path):
                with open(path, 'a') as pipe:
                    utime(path, None)

            if not path in self.__files_cache.keys():
                self.on_append_tab(path)

        dialog.destroy()

        self.set_sensitive(True)


    def on_close_tab(self, button, widget):
        """ Close correctly an existing tab

        Parameters
        ----------
        button : Gtk.Button
            The object which received the signal
        widget : Gtk.Widget
            The tab child widget
        """

        filepath = widget.filepath

        self.logger.debug("Remove notebook tab %s" % basename(filepath))

        # Remove widget from notebook tab
        page = self.notebook.page_num(widget)
        if not page == -1:
            self.notebook.remove_page(page)

        if button is not None:
            del self.__files_cache[filepath]


    def on_modify_editor(self, textbuffer, page):
        """ Synchronize presentation textview with current tab textview

        Parameters
        ----------
        textbuffer : Gtk.TextBuffer
            The object which received the signal
        page : pydv.interface.InterfaceTab
            The object which contains edited textbuffer
        """

        self.presentation.update_file_buffer(page.filepath, page.get_buffer())


    def on_modify_output(self, terminal, page):
        """ Synchronize presentation output with current tab output

        Parameters
        ----------
        terminal : Vte.Terminal
            The object which received the signal
        page : pydv.interface.InterfaceTab
            The object which contains edited Vte
        """

        self.presentation.update_output_buffer(page.get_output_buffer())


    def on_presentation_change_page(self, document, event, pointer):
        """ Emit when the document change page

        Parameters
        ----------
        document : EvinceView.DocumentModel
            The object which received the signal
        """

        self.block_signal()

        self.__current_page = document.get_page()

        self.on_change_page()

        self.unblock_signal()


    def block_signal(self):
        """ Block document scrolling signal
        """

        for widget, signal in self.__signals.items():
            widget.handler_block(signal)

        self.presentation.block_signal()


    def unblock_signal(self):
        """ Unblock document scrolling signal
        """

        for widget, signal in self.__signals.items():
            widget.handler_unblock(signal)

        self.presentation.unblock_signal()


class InterfaceTab(Gtk.Box):

    def __init__(self, parent, filepath):
        """ Constructor

        Parameters
        ----------
        filepath : str
            File path on disk
        """

        Gtk.Box.__init__(self)

        # ------------------------------------
        #   Main variables
        # ------------------------------------

        self.interface = parent
        self.presentation = parent.presentation

        self.config = parent.config

        self.filepath = filepath

        self.profile = parent.profile

        # ------------------------------------
        #   Prepare interface
        # ------------------------------------

        # Init widgets
        self.__init_widgets()

        # Init packing
        self.__init_packing()

        # Init signals
        self.__init_signals()

        # Start interface
        self.__start_interface()


    def __init_widgets(self):
        """ Initialize interface widgets
        """

        # ------------------------------------
        #   Main box
        # ------------------------------------

        self.set_spacing(6)
        self.set_border_width(0)

        self.set_orientation(Gtk.Orientation.VERTICAL)

        # ------------------------------------
        #   Title
        # ------------------------------------

        self.grid = Gtk.Box()

        self.label = Gtk.Label()

        self.image_close = Gtk.Image()
        self.button_close = Gtk.Button()

        # Properties
        self.grid.set_spacing(4)
        self.grid.set_orientation(Gtk.Orientation.HORIZONTAL)

        self.label.set_halign(Gtk.Align.START)

        self.image_close.set_from_icon_name("window-close", Gtk.IconSize.BUTTON)

        self.button_close.set_image(self.image_close)
        self.button_close.set_relief(Gtk.ReliefStyle.NONE)

        # ------------------------------------
        #   Grids
        # ------------------------------------

        self.paned = Gtk.Paned()

        self.grid_editor = Gtk.Box()

        # Properties
        self.paned.set_orientation(Gtk.Orientation.VERTICAL)

        self.grid_editor.set_orientation(Gtk.Orientation.VERTICAL)
        self.grid_editor.set_spacing(0)

        # ------------------------------------
        #   Toolbar
        # ------------------------------------

        self.toolbar = Gtk.Toolbar()

        self.toolbar_item_save = Gtk.ToolButton()
        self.toolbar_item_copy = Gtk.ToolButton()
        self.toolbar_item_cut = Gtk.ToolButton()
        self.toolbar_item_paste = Gtk.ToolButton()
        self.toolbar_item_undo = Gtk.ToolButton()
        self.toolbar_item_redo = Gtk.ToolButton()

        # Properties
        self.toolbar_item_save.set_icon_name("document-save")
        self.toolbar_item_copy.set_icon_name("edit-copy")
        self.toolbar_item_cut.set_icon_name("edit-cut")
        self.toolbar_item_paste.set_icon_name("edit-paste")
        self.toolbar_item_undo.set_icon_name("edit-undo")
        self.toolbar_item_redo.set_icon_name("edit-redo")

        # ------------------------------------
        #   Editor
        # ------------------------------------

        self.scroll = Gtk.ScrolledWindow()

        self.scroll_hadjustment = self.scroll.get_hadjustment()
        self.scroll_vadjustment = self.scroll.get_vadjustment()

        self.editor = GtkSource.View()
        self.editor_buffer = GtkSource.Buffer()

        self.editor_language = GtkSource.LanguageManager()
        self.editor_style = GtkSource.StyleSchemeManager()

        # Properties
        self.editor.set_tab_width(4)
        self.editor.set_top_margin(6)
        self.editor.set_left_margin(12)
        self.editor.set_right_margin(12)
        self.editor.set_bottom_margin(6)
        self.editor.set_monospace(True)
        self.editor.set_show_line_numbers(True)
        self.editor.set_show_right_margin(True)
        self.editor.set_insert_spaces_instead_of_tabs(True)
        self.editor.set_right_margin_position(80)
        self.editor.set_buffer(self.editor_buffer)

        self.editor_buffer.set_style_scheme(
            self.editor_style.get_scheme(self.config.get(
            "interface", "style-scheme", fallback="classic")))

        self.editor.override_font(
            Pango.font_description_from_string(self.config.get(
            "interface", "editor-font", fallback="monospace 12")))
        self.editor.modify_fg(
            Gtk.StateFlags.NORMAL, Gdk.color_parse(self.config.get(
            "interface", "editor-foreground-color", fallback="white")))
        self.editor.modify_bg(
            Gtk.StateFlags.NORMAL, Gdk.color_parse(self.config.get(
            "interface", "editor-background-color", fallback="black")))

        # ------------------------------------
        #   VTE
        # ------------------------------------

        self.grid_vte = Gtk.Box()
        self.grid_vte_execute = Gtk.Box()
        self.grid_vte_compile = Gtk.Box()
        self.grid_vte_show_output = Gtk.Box()

        self.vte = Vte.Terminal()

        self.vte_toolbar = Gtk.Toolbar()

        self.vte_execute = Gtk.ToolItem()
        self.vte_compile = Gtk.ToolItem()
        self.vte_profile = Gtk.ToolItem()
        self.vte_profile_debug = Gtk.ToolItem()
        self.vte_separator = Gtk.SeparatorToolItem()
        self.vte_show_output = Gtk.ToolItem()

        self.vte_execute_button = Gtk.Button()
        self.vte_execute_image = Gtk.Image()
        self.vte_execute_label = Gtk.Label()

        self.vte_compile_button = Gtk.Button()
        self.vte_compile_image = Gtk.Image()
        self.vte_compile_label = Gtk.Label()

        self.vte_profile_combo = Gtk.ComboBox()
        self.vte_profile_store = Gtk.ListStore(str)
        self.vte_profile_cell = Gtk.CellRendererText()
        self.vte_profile_debug_check = Gtk.CheckButton()

        self.vte_show_output_label = Gtk.Label()
        self.vte_show_output_switch = Gtk.Switch()

        # Properties
        self.grid_vte.set_spacing(0)
        self.grid_vte.set_orientation(Gtk.Orientation.VERTICAL)

        self.grid_vte_execute.set_spacing(6)
        self.grid_vte_compile.set_spacing(6)
        self.grid_vte_show_output.set_spacing(12)

        self.vte.set_size(80, 10)
        self.vte.set_allow_bold(True)
        self.vte.set_audible_bell(False)
        self.vte.set_input_enabled(True)
        self.vte.set_rewrap_on_resize(True)
        self.vte.set_cursor_blink_mode(Vte.CursorBlinkMode.SYSTEM)

        self.vte_separator.set_draw(False)
        self.vte_separator.set_expand(True)

        self.vte_execute.set_margin_right(6)
        self.vte_execute_image.set_from_icon_name(
            "media-playback-start-symbolic", Gtk.IconSize.BUTTON)
        self.vte_execute_label.set_label(_("Execute"))
        self.vte_execute_button.get_style_context().add_class(
            "suggested-action")

        self.vte_compile.set_margin_right(6)
        self.vte_compile_image.set_from_icon_name(
            "emblem-system-symbolic", Gtk.IconSize.BUTTON)
        self.vte_compile_label.set_label(_("Compile"))

        self.vte_profile.set_margin_right(6)
        self.vte_profile_store.set_sort_column_id(0, Gtk.SortType.ASCENDING)
        self.vte_profile_combo.set_model(self.vte_profile_store)
        self.vte_profile_combo.pack_start(self.vte_profile_cell, True)
        self.vte_profile_combo.add_attribute(self.vte_profile_cell, "text", 0)

        self.vte_profile_debug_check.set_label(_("Debug ?"))

        self.vte_show_output_label.set_label(_("Show results"))
        self.vte_show_output_label.set_halign(Gtk.Align.END)
        self.vte_show_output_label.set_valign(Gtk.Align.CENTER)
        self.vte_show_output_label.get_style_context().add_class(
            "dim-label")


    def __init_packing(self):
        """ Initialize widgets packing in main window
        """

        self.grid.pack_start(self.label, True, True, 0)
        self.grid.pack_start(self.button_close, False, False, 0)

        self.grid_vte.pack_start(self.vte_toolbar, False, False, 0)
        self.grid_vte.pack_start(self.vte, True, True, 0)

        self.grid_vte_execute.pack_start(
            self.vte_execute_image, False, False, 0)
        self.grid_vte_execute.pack_start(
            self.vte_execute_label, True, True, 0)

        self.grid_vte_compile.pack_start(
            self.vte_compile_image, False, False, 0)
        self.grid_vte_compile.pack_start(
            self.vte_compile_label, True, True, 0)

        self.vte_execute_button.add(self.grid_vte_execute)
        self.vte_compile_button.add(self.grid_vte_compile)

        self.grid_vte_show_output.pack_start(
            self.vte_show_output_label, True, True, 0)
        self.grid_vte_show_output.pack_start(
            self.vte_show_output_switch, False, False, 0)

        self.vte_execute.add(self.vte_execute_button)
        self.vte_compile.add(self.vte_compile_button)
        self.vte_profile.add(self.vte_profile_combo)
        self.vte_profile_debug.add(self.vte_profile_debug_check)
        self.vte_show_output.add(self.grid_vte_show_output)

        self.vte_toolbar.insert(self.vte_execute, -1)
        self.vte_toolbar.insert(self.vte_compile, -1)
        self.vte_toolbar.insert(self.vte_profile, -1)
        self.vte_toolbar.insert(self.vte_profile_debug, -1)
        self.vte_toolbar.insert(self.vte_separator, -1)
        self.vte_toolbar.insert(self.vte_show_output, -1)

        self.scroll.add(self.editor)

        self.toolbar.insert(self.toolbar_item_save, -1)
        self.toolbar.insert(Gtk.SeparatorToolItem(), -1)
        self.toolbar.insert(self.toolbar_item_copy, -1)
        self.toolbar.insert(self.toolbar_item_cut, -1)
        self.toolbar.insert(self.toolbar_item_paste, -1)
        self.toolbar.insert(Gtk.SeparatorToolItem(), -1)
        self.toolbar.insert(self.toolbar_item_undo, -1)
        self.toolbar.insert(self.toolbar_item_redo, -1)

        self.grid_editor.pack_start(self.toolbar, False, False, 0)
        self.grid_editor.pack_start(self.scroll, True, True, 0)

        self.paned.pack1(self.grid_editor, True, False)
        self.paned.pack2(self.grid_vte, True, False)

        self.pack_start(self.paned, True, True, 0)


    def __init_signals(self):
        """ Initialize widgets signals
        """

        self.scroll_hadjustment.connect(
            "value_changed", self.scroll_buffer)
        self.scroll_vadjustment.connect(
            "value_changed", self.scroll_buffer)

        self.toolbar_item_save.connect(
            "clicked", self.__on_save)
        self.toolbar_item_copy.connect(
            "clicked", self.__on_copy)
        self.toolbar_item_cut.connect(
            "clicked", self.__on_cut)
        self.toolbar_item_paste.connect(
            "clicked", self.__on_paste)
        self.toolbar_item_undo.connect(
            "clicked", self.__on_undo)
        self.toolbar_item_redo.connect(
            "clicked", self.__on_redo)

        self.vte.connect(
            "child-exited", self.__on_close_terminal)

        self.vte_compile_button.connect(
            "clicked", self.__on_start_compile)
        self.vte_execute_button.connect(
            "clicked", self.__on_start_execution)

        self.vte_profile_combo.connect(
            "changed", self.__on_selected_profile)

        self.vte_show_output_switch.connect(
            "state-set", self.__on_switch_view)


    def __start_interface(self):
        """ Load data and start interface
        """

        profile = None

        self.label.set_label(basename(self.filepath))

        if self.config.getboolean(
            "interface", "debug-by-default", fallback=False):
            self.vte_profile_debug_check.set_active(True)

        self.editor_buffer.set_language(
            self.editor_language.guess_language(self.filepath))

        with open(self.filepath, 'r') as pipe:
            self.editor_buffer.set_text(''.join(pipe.readlines()))

        # Remove undo stack from GtkSource.Buffer
        if type(self.editor_buffer) is not Gtk.TextBuffer:
            self.editor_buffer.set_undo_manager(None)

        # Retrieve filename extension
        name, extension = splitext(basename(self.filepath))

        # Fill profile combobox
        for section in self.profile.sections():
            # Check if this profile has extensions
            if self.profile.has_option(section, "extensions"):
                extensions = self.profile.get(
                    section, "extensions", fallback=str()).split()

                # Check extension and default profile status
                if extension[1:] in extensions:
                    row = self.vte_profile_store.append([ section ])

                    if self.profile.getboolean(
                        section, "default", fallback=False):
                        profile = row

        # Select matching file profile
        if profile is not None:
            self.vte_profile_combo.set_active_iter(profile)

        self.__on_close_terminal(self.vte, None)

        self.show_all()
        self.grid.show_all()

        self.toolbar_item_undo.set_sensitive(self.editor_buffer.can_undo())
        self.toolbar_item_redo.set_sensitive(self.editor_buffer.can_redo())

        # Rescale paned
        size = self.interface.get_size()
        if len(size) == 2:
            self.paned.set_position(size[1] * 0.6)


    def __on_close_terminal(self, widget, status):
        """

        This function occurs when the user select a profile in the combobox

        Parameters
        ----------
        widget : Gtk.Widget
            The object which receive signal
        status : bool or None
            The child’s exit status
        """

        # Start a default bash to try Vte
        try:
            status, pid = self.vte.spawn_sync(
                Vte.PtyFlags.DEFAULT, dirname(self.filepath), [ "/bin/bash" ],
                list(), GLib.SpawnFlags.DO_NOT_REAP_CHILD, None, None)

            if status is not None and type(status) is bool:
                self.vte.reset(True, True)

        except GLib.Error as error:
            self.logger.exception(
                "An error occurs during Vte spawning child: %s" % str(error))


    def __on_selected_profile(self, widget):
        """ Select a profile

        This function occurs when the user select a profile in the combobox

        Parameters
        ----------
        widget : Gtk.Widget
            The object which receive signal
        """

        treeiter = self.vte_profile_combo.get_active_iter()
        if treeiter is not None:

            section = self.vte_profile_store.get_value(treeiter, 0)
            if section is not None and section in self.profile.sections():

                # Check compilation status
                if self.profile.getboolean(section, "compile", fallback=False):
                    self.vte_compile_button.set_sensitive(True)
                else:
                    self.vte_compile_button.set_sensitive(False)


    def __on_start_compile(self, widget):
        """

        Parameters
        ----------
        widget : Gtk.Widget
            The object which receive signal
        """

        treeiter = self.vte_profile_combo.get_active_iter()
        if treeiter is not None:

            section = self.vte_profile_store.get_value(treeiter, 0)
            if section is not None and section in self.profile.sections():
                command = self.__on_generate_command(section)

                if len(command) > 0:
                    self.vte.feed_child(command, -1)


    def __on_start_execution(self, widget):
        """

        Parameters
        ----------
        widget : Gtk.Widget
            The object which receive signal
        """

        treeiter = self.vte_profile_combo.get_active_iter()
        if treeiter is not None:

            section = self.vte_profile_store.get_value(treeiter, 0)
            if section is not None and section in self.profile.sections():

                # Check interpreter or compile mode
                if self.profile.has_option(section, "compile") and \
                    self.profile.getboolean(section, "compile", fallback=False):
                    command = "./exec\n"

                else:
                    command = self.__on_generate_command(section)

                if len(command) > 0:
                    self.vte.feed_child(command, -1)

                    # Get focus to the Vte to allow user to send input quickly
                    self.vte.grab_focus()


    def __on_generate_command(self, section):
        """ Generate a command from profile configuration file

        Parameters
        ----------
        section : str
            The section to use

        Returns
        -------
        str
            The command string
        """

        command = str()

        if self.profile.has_option(section, "binary"):
            binary = self.profile.get(section, "binary", fallback=str())

            # This binary can be launched
            if len(get_binary_path(binary)) > 0:
                command += "%s " % binary

                # Retrieve specified options
                if self.profile.has_option(section, "options"):
                    command += "%s " % self.profile.get(
                        section, "options", fallback=str())

                # Check debug options
                if self.vte_profile_debug_check.get_active() and \
                    self.profile.has_option(section, "options-debug"):
                    command += "%s " % self.profile.get(
                        section, "options-debug", fallback=str())

            filename = basename(self.filepath)

            name, extension = splitext(basename(filename))

            # Check if the file extension need to be replace
            if self.profile.has_option(section, "replace"):

                for element in self.profile.get(
                    section, "replace", fallback=str()).split():

                    if '→' in element:
                        first, second = element.split('→')

                        # Replace correctly the extention in filename
                        if extension[1:] == first:
                            filename = filename.replace(first, second)

                            # No need to do another replace
                            break

            command += "%s\n" % filename

        return command


    def __on_switch_view(self, widget, status):
        """ Switch the presentation view when user use the toolbar switch

        Parameters
        ----------
        widget : Gtk.Widget
            The object which received the signal
        status : bool
            The new state of the switch
        """

        widget.set_active(status)

        self.presentation.switch_view(status)


    def __on_save(self, widget=None):
        """ Launch a dialog to select a file to open

        Parameters
        ----------
        widget : Gtk.Widget, optional
            Object which received the signal
        """

        # Write buffer to file
        with open(self.filepath, 'w') as pipe:
            pipe.write("%s\n" % self.get_buffer())


    def __on_redo(self, widget=None):
        """ Check buffer content when user redo a changes

        Parameters
        ----------
        widget : Gtk.Widget, optional
            The object which received the signal
        """

        if self.editor_buffer.can_redo():
            self.editor_buffer.redo()

            self.toolbar_item_redo.set_sensitive(self.editor_buffer.can_redo())


    def __on_undo(self, widget=None):
        """ Check buffer content when user undo a changes

        Parameters
        ----------
        widget : Gtk.Widget, optional
            The object which received the signal
        """

        if self.editor_buffer.can_undo():
            self.editor_buffer.undo()

            self.toolbar_item_undo.set_sensitive(self.editor_buffer.can_undo())


    def __on_copy(self, widget=None):
        """ Copy selected buffer text to main clipboard

        Parameters
        ----------
        widget : Gtk.Widget, optional
            The object which received the signal
        """

        if self.editor_buffer.get_has_selection():
            self.editor_buffer.copy_clipboard(self.interface.clipboard)


    def __on_cut(self, widget=None):
        """ Cut selected buffer text to main clipboard

        Parameters
        ----------
        widget : Gtk.Widget, optional
            The object which received the signal
        """

        if self.editor_buffer.get_has_selection():
            self.editor_buffer.cut_clipboard(self.interface.clipboard, True)


    def __on_paste(self, widget=None):
        """ Cut selected buffer text to main clipboard

        Parameters
        ----------
        widget : Gtk.Widget, optional
            The object which received the signal
        """

        self.editor_buffer.paste_clipboard(self.interface.clipboard, None, True)


    def scroll_buffer(self, widget=None):
        """ Scrolling buffer and update presentation buffer to the same position

        Others Parameters
        -----------------
        widget : Gtk.Widget
            The object which received the signal (Default: None)
        """

        if widget is None:
            widget = self.scroll_vadjustment

        orientation = Gtk.Orientation.VERTICAL
        if widget == self.scroll_hadjustment:
            orientation = Gtk.Orientation.HORIZONTAL

        max_value = widget.get_upper() - widget.get_page_size()

        if max_value > 0:
            self.presentation.update_scroll_buffer(
                float((widget.get_value() * 100) / max_value), orientation)


    def get_buffer(self):
        """ Retrieve current text buffer

        Returns
        -------
        str
            Editor text buffer contents
        """

        return self.editor_buffer.get_text(self.editor_buffer.get_start_iter(),
            self.editor_buffer.get_end_iter(), False)


    def get_output_buffer(self):
        """ Retrieve current output text buffer

        Returns
        -------
        str
            Output text buffer contents
        """

        return self.vte.get_text(None)[0]
