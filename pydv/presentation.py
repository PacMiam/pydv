# ------------------------------------------------------------------------------
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 3 of the License.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
# ------------------------------------------------------------------------------

# ------------------------------------------------------------------------------
#   Modules
# ------------------------------------------------------------------------------

# Logging
import logging

# System
from sys import exit as sys_exit

# ------------------------------------------------------------------------------
#   Modules - gobject
# ------------------------------------------------------------------------------

try:
    from gi import require_version

    require_version("Gtk", "3.0")

    from gi.repository import Gtk
    from gi.repository import Gdk
    from gi.repository import Pango

    from gi.repository.GdkPixbuf import Pixbuf
    from gi.repository.GdkPixbuf import Colorspace
    from gi.repository.GdkPixbuf import InterpType

    require_version("GtkSource", "3.0")

    from gi.repository import GtkSource

    require_version("Vte", "2.91")

    from gi.repository import Vte

    require_version("EvinceView", "3.0")

    from gi.repository import EvinceView

except ImportError as error:
    sys_exit("Import error with python3-gobject module: %s" % str(error))

# ------------------------------------------------------------------------------
#   Class
# ------------------------------------------------------------------------------

class PresentationInterface(Gtk.Window):

    def __init__(self, parent):
        """ Constructor
        """

        Gtk.Window.__init__(self)

        # ------------------------------------
        #   Initialize variables
        # ------------------------------------

        self.interface = parent

        self.config = parent.config

        # Interface monitor
        self.monitor = parent.second_monitor
        self.main_monitor = parent.main_monitor

        # Logger
        self.logger = logging.getLogger(__name__)

        # Filename
        self.__current_filepath = None
        self.__current_filebuffer = str()

        # ------------------------------------
        #   Prepare interface
        # ------------------------------------

        # Init widgets
        self.__init_widgets()

        # Init packing
        self.__init_packing()

        # Init signals
        self.__init_signals()

        # Start interface
        self.__start_interface()


    def __init_widgets(self):
        """ Initialize interface widgets
        """

        # ------------------------------------
        #   Main window
        # ------------------------------------

        self.set_wmclass("pydv", "pyDV")

        self.set_position(Gtk.WindowPosition.NONE)
        self.set_gravity(Gdk.Gravity.STATIC)

        self.set_screen(self.monitor.get_screen())

        self.set_transient_for(self.interface)

        # ------------------------------------
        #   Grid
        # ------------------------------------

        self.grid = Gtk.Box()

        # Properties
        self.grid.set_orientation(Gtk.Orientation.VERTICAL)

        # ------------------------------------
        #   Overlay
        # ------------------------------------

        self.overlay = Gtk.Overlay()

        self.fixed_area = Gtk.Fixed()

        self.image_cursor = Gtk.Image()

        # Properties
        self.image_cursor.set_no_show_all(True)
        self.image_cursor.set_from_icon_name("media-record", Gtk.IconSize.MENU)

        # ------------------------------------
        #   Page viewer
        # ------------------------------------

        self.scroll_document = Gtk.ScrolledWindow()

        self.view_presentation = EvinceView.View.new()
        self.model_presentation = EvinceView.DocumentModel()

        # Properties
        self.view_presentation.set_allow_links_change_zoom(False)
        self.view_presentation.modify_bg(
            Gtk.StateFlags.NORMAL, Gdk.color_parse("black"))

        self.model_presentation.set_page_layout(EvinceView.PageLayout.SINGLE)
        self.model_presentation.set_sizing_mode(EvinceView.SizingMode.BEST_FIT)
        self.model_presentation.set_continuous(False)
        self.model_presentation.set_dual_page(False)

        # ------------------------------------
        #   Editor
        # ------------------------------------

        self.scroll_editor = Gtk.ScrolledWindow()

        self.scroll_editor_hadjustment = self.scroll_editor.get_hadjustment()
        self.scroll_editor_vadjustment = self.scroll_editor.get_vadjustment()

        self.editor = GtkSource.View()
        self.editor_buffer = GtkSource.Buffer()

        self.editor_language = GtkSource.LanguageManager()
        self.editor_style = GtkSource.StyleSchemeManager()

        # Properties
        self.editor.set_indent(4)
        self.editor.set_top_margin(6)
        self.editor.set_left_margin(12)
        self.editor.set_right_margin(12)
        self.editor.set_bottom_margin(6)
        self.editor.set_editable(False)
        self.editor.set_monospace(True)
        self.editor.set_cursor_visible(False)
        self.editor.set_buffer(self.editor_buffer)
        self.editor.set_wrap_mode(Gtk.WrapMode.WORD_CHAR)

        self.editor_buffer.set_style_scheme(
            self.editor_style.get_scheme(self.config.get(
            "presentation", "style-scheme", fallback="classic")))

        self.editor.override_font(
            Pango.font_description_from_string(self.config.get(
            "presentation", "editor-font", fallback="monospace 24")))
        self.editor.modify_fg(
            Gtk.StateFlags.NORMAL, Gdk.color_parse(self.config.get(
            "presentation", "editor-foreground-color", fallback="white")))
        self.editor.modify_bg(
            Gtk.StateFlags.NORMAL, Gdk.color_parse(self.config.get(
            "presentation", "editor-background-color", fallback="black")))

        # ------------------------------------
        #   Output
        # ------------------------------------

        self.scroll_output = Gtk.ScrolledWindow()

        self.output = Gtk.TextView()
        self.output_buffer = Gtk.TextBuffer()

        # Properties
        self.output.set_indent(4)
        self.output.set_top_margin(6)
        self.output.set_left_margin(12)
        self.output.set_right_margin(12)
        self.output.set_bottom_margin(6)
        self.output.set_editable(False)
        self.output.set_monospace(True)
        self.output.set_cursor_visible(False)
        self.output.set_buffer(self.output_buffer)
        self.output.set_wrap_mode(Gtk.WrapMode.WORD_CHAR)

        self.output.override_font(
            Pango.font_description_from_string(self.config.get(
            "presentation", "output-font", fallback="monospace 24")))
        self.output.modify_fg(
            Gtk.StateFlags.NORMAL, Gdk.color_parse(self.config.get(
            "presentation", "output-foreground-color", fallback="white")))
        self.output.modify_bg(
            Gtk.StateFlags.NORMAL, Gdk.color_parse(self.config.get(
            "presentation", "output-background-color", fallback="black")))


    def __init_packing(self):
        """ Initialize widgets packing in main window
        """

        self.overlay.add(self.scroll_document)
        self.overlay.add_overlay(self.fixed_area)

        self.scroll_document.add(self.view_presentation)
        self.scroll_editor.add(self.editor)
        self.scroll_output.add(self.output)

        self.grid.pack_start(self.overlay, True, True, 0)
        self.grid.pack_start(self.scroll_editor, True, True, 0)
        self.grid.pack_start(self.scroll_output, True, True, 0)

        self.add(self.grid)


    def __init_signals(self):
        """ Initialize widgets signals
        """

        self.connect(
            "destroy", self.interface.on_stop_interface)

        self.view_presentation.connect(
            "external-link", self.interface.on_external_link_clicked)

        self.signal_page = self.model_presentation.connect(
            "page-changed", self.interface.on_presentation_change_page)


    def __start_interface(self):
        """ Load data and start interface
        """

        self.change_display()

        # Draw a fake laser pointer
        self.fixed_area.put(self.image_cursor, 0, 0)
        self.image_cursor.set_visible(False)


    def block_signal(self):
        """ Block document scrolling signal
        """

        self.model_presentation.handler_block(self.signal_page)


    def unblock_signal(self):
        """ Unblock document scrolling signal
        """

        self.model_presentation.handler_unblock(self.signal_page)


    def init_document(self, document, page=int()):
        """ Define the document model

        Parameters
        ----------
        document : EvinceDocument.Document
            Object which contains document informations
        """

        self.logger.debug("Generate a new model for this document")
        self.model_presentation.set_document(document)

        self.logger.debug("Set this model to evince viewer")
        self.view_presentation.set_model(self.model_presentation)

        # This document start from a specific page
        if page > 0:
            self.update_page(page)


    def update_page(self, page):
        """ Update document page

        Parameters
        ----------
        page : int
            The new document page
        """

        self.model_presentation.set_page(page)


    def change_display(self, monitor=None):
        """ Change the presentation display

        Parameters
        ----------
        monitor : pydv.display.Monitor, optional
            New monitor instance
        """

        # Avoid to unfullscreen if not necessary
        if not self.monitor == monitor:
            self.unfullscreen()

        if monitor is not None:
            self.monitor = monitor

        self.logger.info("Show interface")

        # Avoid to have a fullscreen presentation on main interface monitor
        if self.main_monitor == self.monitor:
            x, y = 0, 0
            width, height = 800, 600

            self.hide()
            self.unrealize()

        # Set correct position on presentation monitor
        else:
            x, y = self.monitor.get_position()
            width, height = self.monitor.get_size()

        # Set interface size from monitor size
        self.set_default_size(width, height)
        self.resize(width, height)

        # Move window into monitor
        self.move(x, y)

        self.logger.debug("Put interface on position %dx%d" % (x, y))

        # Set fullscreen mode for interface in specified monitor
        if not self.main_monitor == self.monitor:
            self.fullscreen_on_monitor(
                self.monitor.get_screen(), self.monitor.get_index())

            # Draw the result on screen
            self.show_all()
            self.scroll_editor.hide()
            self.scroll_output.hide()


    def switch_view(self, output):
        """ Switch between output and editor view
        """

        if output:
            self.scroll_output.show_all()
            self.scroll_editor.hide()

        else:
            self.scroll_editor.show_all()
            self.scroll_output.hide()

        self.overlay.hide()


    def update_scroll_buffer(self, percent, orientation):
        """ Update textview scrollbars

        Parameters
        ----------
        percent : float
            The scroll position
        orientation : Gtk.Orientation
            The modified scrollbar
        """

        if orientation == Gtk.Orientation.VERTICAL:
            widget = self.scroll_editor_vadjustment

        else:
            widget = self.scroll_editor_hadjustment

        widget.set_value(
            ((widget.get_upper() - widget.get_page_size()) * percent) / 100)


    def update_file_buffer(self, filepath, filebuffer):
        """ Update textview buffer

        Parameters
        ----------
        filepath : str
            The new file path
        filebuffer : str
            The new file buffer content
        """

        if not self.__current_filebuffer == filebuffer or \
            not self.__current_filepath == filepath:
            self.__current_filepath = filepath
            self.__current_filebuffer = filebuffer

            self.editor_buffer.set_language(
                self.editor_language.guess_language(filepath))

            self.editor_buffer.set_text(filebuffer)


    def update_output_buffer(self, outputbuffer):
        """ Update output textview buffer

        Parameters
        ----------
        outputbuffer : str
            The new output buffer content
        """

        if not self.output_buffer.get_text(self.output_buffer.get_start_iter(),
            self.output_buffer.get_end_iter(), False) == outputbuffer:
            self.output_buffer.set_text(outputbuffer)


    def move_cursor(self, x, y):
        """ Move the cursor to a specific area

        Parameters
        ----------
        x : int
            Horizontal position
        y : int
            Vertical position
        """

        if not self.image_cursor.is_visible():
            self.image_cursor.set_visible(True)

        area, baseline = self.view_presentation.get_allocated_size()

        x *= area.width
        y *= area.height

        if x >= 0 and y >= 0 and x < area.width and y < area.height:
            self.fixed_area.move(self.image_cursor, int(x), int(y))


    def set_visibility(self, visible):
        """ Update presentation widgets visibility

        Parameters
        ----------
        visible : Gtk.Widget
            The widget which gain visibility
        """

        widgets = [
            self.overlay,
            self.scroll_editor,
            self.scroll_output
        ]

        if visible in widgets:
            visible.show_all()

            for widget in widgets:
                if not widget == visible:
                    widget.hide()
