# ------------------------------------------------------------------------------
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 3 of the License.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
# ------------------------------------------------------------------------------

# ------------------------------------------------------------------------------
#   Modules
# ------------------------------------------------------------------------------

# Filesystem
from os import mkdir

from os.path import exists
from os.path import dirname
from os.path import expanduser
from os.path import join as path_join

# Logging
import logging
from logging.config import fileConfig

# System
from argparse import ArgumentParser

# ------------------------------------------------------------------------------
#   Modules - pyDV
# ------------------------------------------------------------------------------

from pydv import PyDV

from pydv.utils import get_data
from pydv.application import Application

# ------------------------------------------------------------------------------
#   Launcher
# ------------------------------------------------------------------------------

def main():
    """ Main launcher
    """

    # Generate default arguments
    parser = ArgumentParser(
        prog=PyDV.NAME,
        description=PyDV.DESCRIPTION,
        epilog=PyDV.EPILOG,
        conflict_handler="resolve")

    parser.add_argument("document",
        type=str,
        nargs='?',
        action="store",
        metavar="FILE",
        help="the document to show on specified monitor")

    parser.add_argument("-v", "--version",
        action="version",
        version=PyDV.SHORTDESCRIPTION,
        help="show the current version")

    parser.add_argument("-d", "--debug",
        action="store_true",
        help="set the debug mode")

    # ----------------------------------------
    #   Interface
    # ----------------------------------------

    parser_interface = parser.add_argument_group("interface arguments")

    parser_interface.add_argument("-D", "--display",
        type=int,
        action="store",
        metavar="ID",
        default=int(),
        help="set the presentation display (Default: 0)")

    parser_interface.add_argument("-m", "--monitor",
        type=str,
        action="store",
        metavar="NAME",
        default=None,
        help="set the presentation viewer monitor")

    parser_interface.add_argument("-M", "--main-monitor",
        type=str,
        action="store",
        metavar="NAME",
        default=None,
        help="set the default monitor for main interface")

    parser_interface.add_argument("-l", "--list-monitors",
        action="store_true",
        help="list available monitors")

    parser_interface.add_argument("-t", "--timer",
        type=str,
        action="store",
        metavar="HH:MM",
        default=str(),
        help="set the default timer")

    # ----------------------------------------
    #   Document
    # ----------------------------------------

    parser_document = parser.add_argument_group("document arguments")

    parser_document.add_argument("-p", "--page",
        type=int,
        action="store",
        metavar="INT",
        default=1,
        help="set the default page for document opening")

    # ----------------------------------------
    #   Launcher
    # ----------------------------------------

    arguments = parser.parse_args()

    # Generate pyDV folders
    PyDV.Path.check()

    # Define log path with a global variable
    logging.log_path = PyDV.Path.LOG

    # Generate logger from log.conf
    fileConfig(get_data(path_join("config", "log.conf")))

    logger = logging.getLogger("pydv")

    if not arguments.debug:
        logger.setLevel(logging.INFO)

    # Start application
    Application(arguments)


if __name__ == "__main__":
    main()
