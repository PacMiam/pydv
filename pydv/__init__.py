# ------------------------------------------------------------------------------
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 3 of the License.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
# ------------------------------------------------------------------------------

# ------------------------------------------------------------------------------
#   Modules
# ------------------------------------------------------------------------------

# Filesystem
from os import mkdir

from os.path import exists
from os.path import expanduser
from os.path import join as path_join

# ------------------------------------------------------------------------------
#   Modules - pyDV
# ------------------------------------------------------------------------------

from pydv.utils import get_data

# ------------------------------------------------------------------------------
#   Modules - XDG
# ------------------------------------------------------------------------------

try:
    from xdg.BaseDirectory import xdg_data_home
    from xdg.BaseDirectory import xdg_config_home

except ImportError as error:
    from os import environ

    if "XDG_DATA_HOME" in environ:
        xdg_data_home = environ["XDG_DATA_HOME"]
    else:
        xdg_data_home = expanduser("~/.local/share")

    if "XDG_CONFIG_HOME" in environ:
        xdg_config_home = environ["XDG_CONFIG_HOME"]
    else:
        xdg_config_home = expanduser("~/.config")

# ------------------------------------------------------------------------------
#   Data
# ------------------------------------------------------------------------------

class PyDV(object):
    """ pyDV data
    """

    NAME = "pyDV"
    VERSION = "0.1"
    LICENSE = "GPLv3"

    DESCRIPTION = """
    pyDV is a tool to help teachers to manage their presentations with
    multiscreens
    """

    SHORTDESCRIPTION = "%s %s - License %s" % (NAME, VERSION, LICENSE)

    EPILOG = "Copyleft %s team - License %s" % (NAME, LICENSE)

    class Path(object):
        """ pyDV path
        """

        BASE_DATA = path_join(xdg_data_home, "pydv")
        BASE_CONFIG = path_join(xdg_config_home, "pydv")

        LOG = path_join(BASE_DATA, "pydv.log")

        CONFIG = path_join(BASE_CONFIG, "pydv.conf")
        PROFILE = path_join(BASE_CONFIG, "profile.conf")

        def check():
            """ Check missing folders and generate them
            """

            if not exists(xdg_data_home):
                mkdir(xdg_data_home)

            if not exists(xdg_config_home):
                mkdir(xdg_config_home)

            # ----------------------------------------
            #   Specific pyDV folders
            # ----------------------------------------

            if not exists(PyDV.Path.BASE_DATA):
                mkdir(PyDV.Path.BASE_DATA)

            if not exists(PyDV.Path.BASE_CONFIG):
                mkdir(PyDV.Path.BASE_CONFIG)
