# ------------------------------------------------------------------------------
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 3 of the License.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
# ------------------------------------------------------------------------------

# ------------------------------------------------------------------------------
#   Modules
# ------------------------------------------------------------------------------

from collections import OrderedDict

# ------------------------------------------------------------------------------
#   Class
# ------------------------------------------------------------------------------

class Display(object):

    def __init__(self, display):
        """ Constructor

        Parameters
        ----------
        display : Gdk.Display
            the x11 display
        """

        # ----------------------------------------
        #   Variables
        # ----------------------------------------

        self.__display = display

        self.__name = display.get_name()

        self.__screen = display.get_default_screen()

        self.__primary = self.__screen.get_primary_monitor()
        self.__monitors = OrderedDict()

        self.__width = self.__screen.get_width()
        self.__height = self.__screen.get_height()

        for index in range(self.__screen.get_n_monitors()):
            monitor = Monitor(self.__screen, index)

            self.__monitors[monitor.get_name()] = monitor


    def __str__(self):
        """ Print object data

        Returns
        -------
        str
            Formatted string
        """

        text = "%s [%d×%d]" % (self.__name, self.__width, self.__height)

        for monitor in self.__monitors.values():
            text += "\n| %s" % monitor

        return text


    def get_name(self):
        """ Get display name

        Returns
        -------
        str
            display name
        """

        return self.__name


    def get_display(self):
        """ Get display object

        Returns
        -------
        Gdk.Display
            display object
        """

        return self.__display


    def get_screen(self):
        """ Get display screen object

        Returns
        -------
        Gdk.Screen
            display screen object
        """

        return self.__screen


    def get_size(self):
        """ Get display screen size

        Returns
        -------
        tuple
            display screen size as integer tuple
        """

        return (self.__width, self.__height)


    def get_default_monitor(self):
        """ Get default monitor

        This function return the monitor with primary status

        Returns
        -------
        pydv.display.Monitor
            default monitor objet
        """

        return list(self.__monitors.values())[self.__primary]


    def get_monitor(self, name):
        """ Get a specific monitor

        Parameters
        ----------
        name : str
            Monitor name

        Returns
        -------
        pydv.display.Monitor
            default monitor objet

        Examples
        --------
        >>> self.get_monitor("DP-3")
        <pydv.display.Monitor object at 0x51ca81ab59de>
        """

        if type(name) is not str:
            raise TypeError("Expected str, got %s" % str(type(name)))

        if not name in self.__monitors:
            raise KeyError("Monitors has no key %s" % str(name))

        return self.__monitors[name]


    def get_monitors(self):
        """ Get available monitors

        This function return all the monitor

        Returns
        -------
        list
            available monitors as list
        """

        return list(self.__monitors.values())


class Monitor(object):

    def __init__(self, screen, index):
        """ Constructor

        Parameters
        ----------
        screen : Gdk.Screen
            Monitor screen
        index : int
            Monitor index
        """

        # ----------------------------------------
        #   Variables
        # ----------------------------------------

        self.__index = index
        self.__screen = screen

        self.__area = screen.get_monitor_geometry(index)
        self.__name = screen.get_monitor_plug_name(index)

        # Coordinates
        self.__x = self.__area.x
        self.__y = self.__area.y

        # Size
        self.__width = self.__area.width
        self.__height = self.__area.height

        # Primary
        self.__primary = False


    def __str__(self):
        """ Print object data

        Returns
        -------
        str
            Formatted string
        """

        default = str()
        if self.__primary:
            defautl = "(Default)"

        return "%-5s %5d×%-5d [%d×%d]" % (
            self.__name, self.__width, self.__height, self.__x, self.__y)


    def get_screen(self):
        """ Returns monitor screen object

        Returns
        -------
        Gdk.Screen
            screen object
        """

        return self.__screen


    def get_index(self):
        """ Returns monitor index

        Returns
        -------
        int
            monitor index
        """

        return self.__index


    def get_name(self):
        """ Returns monitor name

        Returns
        -------
        str
            monitor name
        """

        return self.__name


    def set_position(self, x, y):
        """ Set monitor position

        Parameters
        ----------
        x : int
            Monitor new horizontal position
        y : int
            Monitor new vertical position
        """

        self.__x = x
        self.__y = y


    def get_position(self):
        """ Returns monitor position

        Returns
        -------
        tuple
            Tuple contains monitor x and y
        """

        return (self.__x, self.__y)


    def set_size(self, width, height):
        """ Set monitor size

        Parameters
        ----------
        widht: int
            Monitor new width
        height: int
            Monitor new height
        """

        self.__width = width
        self.__height = height


    def get_size(self):
        """ Return monitor size

        Returns
        -------
        tuple
            Tuple contains monitor width and height
        """

        return (self.__width, self.__height)


    def set_primary(self, status):
        """ Set the monitor primary status

        Parameters
        ----------
        status : bool
            Primary status
        """

        self.__primary = status


    def get_primary(self):
        """ Returns monitor primary status

        Returns
        -------
        bool
            Primary status as boolean
        """

        return self.__primary
