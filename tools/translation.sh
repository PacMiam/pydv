#!/bin/bash

translation=(fr)

# Create .pot files
for lang in "${translation[@]}" ; do
    if [ ! -d pydv/i18n/$lang ] ; then
        mkdir -p pydv/i18n/$lang
    fi

    if [ ! -f pydv/i18n/$lang/pydv.po ] ; then
        msginit -i pydv/i18n/pydv.pot -o pydv/i18n/$lang/pydv.po
    fi
done

# Generate .po files
xgettext -k_ -i --strict -s --omit-header -o pydv/i18n/pydv.pot \
    --copyright-holder="Kawa Team" --package-name=pydv --from-code=utf-8 \
    --package-version="0.1" pydv/*.py

for lang in "${translation[@]}" ; do
    msgmerge -s -U pydv/i18n/$lang/pydv.po pydv/i18n/pydv.pot

    if [ ! -d pydv/i18n/$lang/LC_MESSAGES ] ; then
        mkdir -p pydv/i18n/$lang/LC_MESSAGES
    fi

    msgfmt pydv/i18n/$lang/pydv.po -o pydv/i18n/$lang/LC_MESSAGES/pydv.mo
done
