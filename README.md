                    ___
     _ __  _   _   /   \\   /\
    | '_ \| | | | / /\ \ \ / /
    | |_) | |_| |/ /_// \ V /
    | .__/ \__, /___,'   \_/
    |_|    |___/
                    License GPLv3

A nice and powerful PDF presentation application for GTK+ environment

Dependencies
------------

 * python3
 * python3-gobject
 * gtksourceview
 * evince
 * vte3

Command
-------

The following commands need to be launch in pydv root folder (where this file
can be found)

```
python3 -m pydv test.pdf
```

Arguments list can be obtain with --help

```
python3 -m pydv --help
```

Features
--------

- Download source code file and show them in presentation view
- Execute source code file and show the result in presentation view
- Freezing the presentation view
- Highlight annotations availables
- Set the presentation view monitor from main view
- Show slide thumbnails and allow to navigate between them
- Timer and clock in main view
- Virtual Terminal Emulator available in presentation view

Todo
----

- Add keyboard shortcuts

Fix
---
- Document tab is not correctly update when focus on a script tab
- "Show output" switch in tab can be used in freezed mode
